// endpoint.mw.mjs
// 21th april 2024

// Middleware
// notice that parameter next means to go to next request handler inside e.g router.mjs

/** @type { import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    // todo...
    console.log("Middleware works...");
    next();
};

export default endpoint_mw;