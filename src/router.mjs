// router.mjs
// 21th april 2024

import express from "express";
import Counter from './counter.mjs';
import logger from "./logger.mjs";
import endpoint_mw from "./endpoint.mw.mjs";

// Counter
const app = express();
const counter = new Counter();

// Middleware
app.use(endpoint_mw);

// ------------------------------------
// Endpoints:

// at arrival to website
app.get('',(req, res) => {
    logger.http("[ENDPOINT] GET '/'");
    res.send("Welcome!")
});

// GET /counter-increase
app.get('/counter-increase', (req, res) =>{
    logger.http("[ENDPOINT] GET '/counter-increase'");
    counter.increase();
    res.send(String(counter.read()));
});

// GET /counter-read
app.get('/counter-read', (req, res) =>{
    logger.http("[ENDPOINT] GET '/counter-read'");
    const url = req.url; // mikä tuo on?
    const method = req.method; // voiko hyödyntää?
    res.send(String(counter.read()));
});

// GET /counter-zero
app.get('/counter-zero', (req, res) =>{
    logger.http("[ENDPOINT] GET '/counter-zero'")
    counter.zero();
    res.send(String(counter.read()));
});


// REQUEST non-existing
// '*' means that path doesnt matter
app.all('*', (req, res) => {
    logger.http(`[ENDPOINT] ${req.method} '/non-existing'`);
    res.status(404).send("Resource not found.");
});

export default app;