// logger.mjs
// 21th april 2024

import { createLogger, transports, format } from "winston";

export default createLogger({
  level: 'http',
  format: format.combine(
    format.timestamp(),
    format.json()
  ),
  transports: [
    new transports.Console(),
    new transports.File({
      filename: 'logs/error.log',
      level: 'error'
    }),
    new transports.File({
      filename: 'logs/combined.log'
    })
  ]
});