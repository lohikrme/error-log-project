// main.mjs
// 20th april 2024

import logger from "./logger.mjs";
import app from "./router.mjs";
import http from 'http';

// Router
const PORT = 3000;
const PORT2 = 3001;
const HOSTNAME = "127.0.0.1";

// create two servers with one Express app

http.createServer(app).listen(PORT, HOSTNAME, () => {
    logger.log("info", `listening at http://${HOSTNAME}:${PORT}`);
});

http.createServer(app).listen(PORT2, HOSTNAME, () => {
    logger.log("info", `listening at http://${HOSTNAME}:${PORT2}`);
});

const main = () => {

}
logger.log("info","Service is starting...");
// test if error works
logger.log("error", "This is an error.");

main();