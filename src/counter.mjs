// counter.mjs
// 21th april 2024

class Counter {
    constructor() {
        this.count = 0;
    }

    read() {
        return this.count;
    }

    increase() {
        this.count += 1;
    }

    zero() {
        this.count = 0;
    }

}


export default Counter;