// counter.spec.mjs
// 21th april 2024

import { describe, it} from "mocha";
import { expect } from 'chai';
import Counter from "../src/counter.mjs";

let counter = new Counter();

describe('Counter tests', () => {
    it('Initial counter count is 0', () => {
        counter = new Counter();
        expect(counter.count).to.equal(0);
    });
    it('Counter count can be increased', () => {
        counter = new Counter();
        counter.increase();
        expect(counter.count).to.equal(1);
    });
    it('Counter count can be increased and zeroed', () => {
        counter = new Counter();
        counter.increase();
        expect(counter.read()).to.equal(1);
        counter.zero();
        expect(counter.count).to.equal(0);
    });
});